
<nav class="col-md-2 d-none d-md-block bg-light sidebar">
      <div class="sidebar-sticky">
        <ul class="nav flex-column">
          
          <li class="nav-item">
            <a class="nav-link active" href="{{route('admin')}}">
              <span data-feather="home"></span>
              Admin <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('user')}}">
              <span data-feather="file"></span>
              User
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('booking')}}">
              <span data-feather="shopping-cart"></span>
              Booking
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('location')}}">
              <span data-feather="users"></span>
              Location
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('trip')}}">
              <span data-feather="users"></span>
              Trip
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('counter')}}">
              <span data-feather="users"></span>
              Counter
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('coach')}}">
              <span data-feather="users"></span>
              Coach
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('payment')}}">
              <span data-feather="users"></span>
              Payment
            </a>
          </li>          
          <li class="nav-item">
            <a class="nav-link" href="#">
              <span data-feather="bar-chart-2"></span>
              Reports
            </a>
          </li>
        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
          
        </h6>
        <ul class="nav flex-column mb-2">
          
        </ul>
      </div>
    </nav>