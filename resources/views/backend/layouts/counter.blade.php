  @extends('backend.master')
  @section('content')
  <h1>Counter</h1>
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
    Add Counter
  </button><br>

  <!-- Step-4.2-(session) -->
  @if(session()->has('kodeeo'))
  <div class="alert alert-success">
    {{ session()->get('kodeeo') }}
  </div>
  @endif

  <!-- Table -->
  <div class="table-responsive">

   <br><table id="mytable">

    <thead>
      <tr>
       <th scope="col">ID.</th>
       <th scope="col">Counter_Name</th>
       <th scope="col">Location_ID</th>
       <th scope="col">Counter_Address</th>
       <th scope="col">Mobile_Number</th>
       <th scope="col">Action</th>
     </tr>
   </thead>

   <tbody>
     @foreach($counter as $data) 
     <tr>
      <td>{{$data->id}}</td>  
      <td>{{$data->name}}</td>  
      <td>{{$data->location->name}}</td>  
      <td>{{$data->address}}</td>  
      <td>{{$data->mobile}}</td>  
      <td>
        <a href="" class="btn btn-success" data-toggle="modal" data-target="#editModal">Edit</a>

        @method('delete')
        @csrf
        <a href="{{route('counter.delete',$data->id)}}" class="btn btn-danger" data-method="delete">Delete</a>
      </td>  

      <!------------------EDIT MODAL--------------------------------------->
      <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Create New </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form method="POST" action="{{route('counter.update',$data->id)}}" >
              @csrf
              
              <!-- Step-4.2-(session) -->
              @if(session()->has('kodeeo'))
              <div class="alert alert-success">
                {{ session()->get('kodeeo') }}
              </div>
              @endif
              <div class="modal-body">

                <label> Enter Counter Name</label>
                <input class="form-control" type="text" name="counter_name" placeholder="Enter Counter Name.." value="{{$data->name}}" >

                <label> Enter Counter Address</label>
                <input class="form-control" type="text" name="counter_address" placeholder="Enter Counter Address.." value="{{$data->address}}">

                <label> Enter Counter Mobile Number</label>
                <input class="form-control" type="number" name="counter_mobile_number" placeholder="Enter Mobile Number.." value="{{$data->mobile}}" >

                <label>Select Counter Location</label>
                <select class="form-control" class="form-control" name="location_id">
                  <option value=""></option>
                  
                </select>
              </div>
              <div class="modal-footer">

                <button type="submit" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!------------------EDIT MODAL--------------------------------------->
  </tr>
  @endforeach
  </tbody>


  </table>

  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Create New </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{route('counter.create')}}" >
          @csrf
          <div class="modal-body">

            <label> Enter Counter Name</label>
            <input class="form-control" type="text" name="counter_name" placeholder="Enter Counter Name.." >

            <label> Enter Counter Address</label>
            <input class="form-control" type="text" name="counter_address" placeholder="Enter Counter Address.." >

            <label> Enter Counter Mobile Number</label>
            <input class="form-control" type="number" name="counter_mobile_number" placeholder="Enter Mobile Number.."  >

            <label>Select Counter Location</label>
             <select class="form-control" name="location_id">
              @foreach($location as $data)
              <option value="{{$data->id}}">{{$data->name}}</option>
              @endforeach
            </select>


          </div>
          <div class="modal-footer">

            <button type="submit" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

          </div>
        </form>
      </div>
    </div>
  </div>
  </div>
  @stop
