@extends('backend.master')
@section('content')
<h1>Trip</h1>

<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModal"> Add </button><br>

<!-- Step-4.2-(session) -->
@if(session()->has('kodeeo'))
<div class="alert alert-success"> 
  {{ session()->get('kodeeo') }}
</div>
@endif

<br>

<!-- Table -->
<div class="table-responsive">
    <table class="table" id="mytable">
        <thead>
            <tr>
                <th scope="col">Serial No.</th>
                <th scope="col">Location ID</th>
                <th scope="col">Counter ID</th>
                <th scope="col">Price</th>
                <th scope="col">Reporting Time</th>
                <th scope="col">Departure Time</th>
                <th scope="col">Reporting Counter Name</th>
                <th scope="col">Action</th>
            </tr>
        </thead>

        <tbody> 
            @foreach($trip as $key=>$data)

            <tr>
                <td>{{$key+1}}</td>
                <td>{{$data->location->name}}</td>
                <td>{{$data->location->counter->name}}</td>
                <td>{{$data->price}}</td>
                <td>{{$data->reporting_time}}</td>
                <td>{{$data->departure_time}}</td>
                <td>{{$data->departure_counter_id}}</td>
                <td>
                 <a href="" data-toggle="modal" data-target="#editModal{{$data->id}}" class="btn btn-success" >Edit</a>
                 @method('delete')
                 @csrf
                 <a href="{{route('trip.delete',$data->id)}}" class="btn btn-danger" data-method="delete">Delete</a>
             </td>
         </tr>

         
         <!-------- Edit MODAL -------------------------->
         
         <div class="modal" id="editModal{{$data->id}}">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Edit Trip Details</h5>
                <button class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

                <form method="POST" action="{{route('trip.update',$data->id)}}">
                   
                    @csrf
                    
                    <!-- Step-4.2-(session) -->
                    @if(session()->has('kodeeo'))
                    <div class="alert alert-success">
                      {{ session()->get('kodeeo') }}
                  </div>
                  @endif
                  <div class="form-group">
                    <label for="location">Location Name</label>
                    <select name="location" class="form-control" style="width:250px">
                     @foreach($location as $key=>$value)
                     <!-- {{$value}} -->
                     <option value="{{$key}}">{{$value}}</option>
                     @endforeach
                 </select>
             </div>

             <div class="form-group">
                <label for="counter">Counter Name</label>
                <select name="counter_id" class="form-control" style="width:250px">

                </select>
            </div>

            <div class="form-group">
                <label for="">Price</label>
                <input class="form-control" type="text" placeholder="Price" name="price" value="{{$data->price}}">
            </div>

            <div class="form-group">
                <label for="">Reporting Time</label>
                <input class="form-control" type="time" placeholder="Reporting Time" name="r_time" value="{{$data->reporting_time}}">
            </div>
            <div class="form-group">
                <label for="">Departure Time</label>
                <input class="form-control" type="time" placeholder="Reporting Time" name="d_time" value="{{$data->departure_time}}">
            </div>

            <div class="form-group">
                <label for="dep_rep_name">Reporting Counter Name</label>
                <select name="dp_counter" class="form-control" style="width:250px">
                    
                </select>

            </div>

            <div class="modal-footer">
                <button class="btn btn-success" type="submit">Save</button>
                <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                
            </div>

        </form>
    </div>
    
</div>
</div>
</div>

</div>

@endforeach

</tbody> 
</table>

<!---------------Add MODAL  ------------------------------------------>

<div class="modal" id="addModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Trip ADD</h5>
        <button class="close" data-dismiss="modal">&times;</button>
    </div>
    <div class="modal-body">
        <form method="POST" action="{{route('trip.add')}}">
            @csrf
            <div class="form-group">
                <label for="location">Location Name</label>
                <select name="location" class="form-control" style="width:250px">
                    <option value="">--- Select Location ---</option>
                    @foreach ($location as $key => $value)
                    <option value="{{ $key }}">{{ $value }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="counter">Select Counter</label>

                <select name="counter_id" id="counter" class="form-control">
                    
                </select>


            </div>

            <div class="form-group">
                <label for="">Price</label>
                <input class="form-control" type="text" placeholder="Price" name="price">
            </div>
            <div class="form-group">
                <label for="">Reporting Time</label>
                <input class="form-control" type="time" placeholder="Reporting Time" name="r_time">
            </div>
            <div class="form-group">
                <label for="">Departure Time</label>
                <input class="form-control" type="time" placeholder="Departure Time" name="d_time">
            </div>

            <div class="form-group">
                <label for="dep_rep_name">Reporting Counter Name</label>
                <select name="dc_name" class="form-control" style="width:250px">
                    <option value="">-Reporting Counter Name-</option>
                    
                    @foreach($counter as $data)
                    <option value="{{$data->id}}">{{$data->name}}</option>
                    @endforeach

                </select>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" type="submit">Save</button>
                <button class="btn btn-secondary" data-dismiss="modal">Close</button>

            </div>
        </form>

    </div>

</div>
</div>
</div>

</div>

<!---------------Add MODAL  ------------------------------------------>

@stop


@push('scripts')


<script type="text/javascript">
    $(document).ready(function ()
    {
        var locations =$('select[name="location"]');
        locations.on('change',function(){
         var locationsID = jQuery(this).val();
         var url ='{{url('/cut?id=')}}'+locationsID;
         var urlrp ='{{url('/rep?id=')}}'+locationsID;
         $.ajax(
         {
            url: url, 

            success: function(result){
               var data =JSON.parse(result);
               var s ='<option value="">--Select counter--</option>';
               var counters=$('select[name="counter_id"]');
               
               $.each( data, function( key, value ) {

                s+='<option value="'+key+'">'+value+'</option>';  
                
            });

               counters.html(s);

           }});


         $.ajax(
         {
            url: urlrp, 

            success: function(result){
               var data =JSON.parse(result);
               var e ='<option value="">--Select counter--</option>';
               var rp_counters=$('select[name="dp_counter"]');
               
               $.each( data, function( key, value ) {

                e+='<option value="'+key+'">'+value+'</option>';  
                
            });

               rp_counters.html(e);

           }});

         

     });

        
    });
</script>



@endpush