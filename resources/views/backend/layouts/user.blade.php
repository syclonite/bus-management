@extends('backend.master')
@section('content')
<h1>Users</h1>


<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
	Add 
</button><br>

<!-- Step-4.2-(session) -->
@if(session()->has('kodeeo'))
<div class="alert alert-success">
  {{ session()->get('kodeeo') }}
</div>
@endif

<!-- Table -->
<div class="table-responsive"><br>
	<table class="table" id="mytable">
   <thead>
    <tr>
     <th scope="col">No.</th>  
     <th scope="col">User Id</th>
     <th scope="col">User Type</th>
     <th scope="col">User Name</th>
     <th scope="col">Email</th>
     <th scope="col">Mobile</th>
     <th scope="col">Addres</th>
     <th scope="col">Image</th>
     <th scope="col">Action</th>
   </tr>
 </thead>


 <tbody>
  @foreach($user as $key=>$data)
  <tr>
    <td>{{$key+1}} </td>
    <td>{{$data->id}}</td>
    <td>{{$data->role}}</td>
    <td>{{$data->name}}</td>
    <td>{{$data->email}}</td>
    <td>{{$data->mobile}}</td>
    <td>{{$data->address}}</td>
    <td></td>

    <td>
     <a href="" data-toggle="modal" data-target="#editModal{{$data->id}}" class="btn btn-success" >Edit</a>
     
     @method('delete')
     @csrf
     <a href="{{route('user.delete',$data->id)}}" class="btn btn-danger" data-method="delete" >Delete</a>

   </td>

   <!----------------Edit Modal------------------------> 



   <div class="modal" id="editModal{{$data->id}}">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">User Update</h5>
          <button class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <!-- Step-4.2-(session) -->
          @if(session()->has('kodeeo'))
          <div class="alert alert-success">
            {{ session()->get('kodeeo') }}
          </div>
          @endif
          <form method="POST" action="{{route('user.update',$data->id)}}">
            @csrf
            <div class="form-group">
              <label for="">Name</label>
              <input class="form-control" type="text" placeholder="Your Name" name="name" value="{{$data->name}}">
            </div>
            <div class="form-group">
              <label for="">E-mail</label>
              <input class="form-control" type="email" placeholder="Email" name="email" value="{{$data->email}}">
            </div>

            <div class="form-group">            
              <label for="address">Address:</label>            
              <input class="form-control" type="text" name="address" placeholder="Enter address" value="{{$data->address}}" />
            </div>
            <div class="form-group">            
              <label class="">Phone:</label>            
              <input class="form-control" type="text" name="phone" placeholder="Enter contact" value="{{$data->mobile}}"  required/>       
            </div>
            <div class="modal-footer">
              <button class="btn btn-success" type="submit">Save</button>
              <button class="btn btn-secondary" data-dismiss="modal">Close</button>

            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<!----------------Edit Modal------------------------> 
</tr>
@endforeach
</tbody>  
</table>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create New User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="{{route('user.create')}}">
        @csrf
        <div class="modal-body">
          <!-- dropdown -->
          <label>Select User Type</label>
          <select class="form-control" name="type" >
            <option>Select</option>
            <option value="customer">Customer</option>
            <option value="employee">Employee</option>
            
          </select>

          <label>	Enter Users Name</label>
          <input class="form-control" type="text" name="name" placeholder="Enter users Name Here.." >

          <label>	Enter Email</label>
          <input class="form-control" type="email" name="email" placeholder="Enter email Here.." >

          <label>	Enter Mobile Number</label>
          <input class="form-control" type="number" name="phone" placeholder="Enter mobile number Here.." >

          <label> Password</label>
          <input class="form-control" type="password" name="password" placeholder="password" >

          <label>	Enter Addres</label>
          <input class="form-control" type="text" name="address" placeholder="Enter Addres Here.." >

        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
         <button type="submit" class="btn btn-primary">Save</button>
       </div>
     </form>
   </div>
 </div>
</div>

@stop