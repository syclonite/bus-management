@extends('backend.master')
@section('content')

<h1>Coach</h1>    

<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Add 
</button><br>

<!-- Step-4.2-(session) -->
@if(session()->has('kodeeo'))
<div class="alert alert-success">
  {{ session()->get('kodeeo') }}
</div>
@endif

<div class="table-responsive"><br>
  <table class="table" id="mytable">

   <thead>
    <tr>
     <th scope="col">Serial No.</th>  
     <th scope="col">Type</th>
     <th scope="col">Number</th>
     <th scope="col">Action</th>
   </tr>
 </thead>


 <tbody>
  @foreach($coach as $key=>$data)
  <tr>
    <td>{{$key+1}}</td>
    <td>{{$data->type}}</td>
    <td>{{$data->number}}</td>
    <td>
     <a href="" data-toggle="modal" data-target="#editModal{{$data->id}}" class="btn btn-success" >Edit</a>
     
     @method('delete')
     @csrf
     <a href="{{route('coach.delete',$data->id)}}" class="btn btn-danger" data-method="delete" >Delete</a>

   </td>

   <!----------------Edit Modal------------------------> 

   <div class="modal" id="editModal{{$data->id}}">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"> Update</h5>
          <button class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form method="POST" action="{{route('coach.update',$data->id)}}">
            @csrf
            
            <!-- Step-4.2-(session) -->
            @if(session()->has('kodeeo'))
            <div class="alert alert-success">
              {{ session()->get('kodeeo') }}
            </div>
            @endif
            <div class="form-group">
              
              <select name="type" class="form-control">
                <option value="">SELECT</option>
                <option value="non_ac">NON-AC</option>
                <option value="ac">AC</option>
                
              </select> 

            </div>

            <div class="form-group">            
              <label for="number">Number</label>            
              <input class="form-control" type="number" name="number" placeholder="Enter Number" value="{{$data->number}}" />
            </div>
            
            <div class="modal-footer">
              <button class="btn btn-success" type="submit">Save</button>
              <button class="btn btn-secondary" data-dismiss="modal">Close</button>

            </div>

          </form>


        </div>


      </div>
    </div>
  </div>

</div>

<!----------------Edit Modal------------------------> 

</tr>
@endforeach

</tbody>  

</table>

</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Create New Coach</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="{{route('coach.create')}}">
        @csrf
        <div class="modal-body">
          <!-- dropdown -->
          <label>Coach Type</label>
          <select class="form-control" name="type" >
            <option value="">Select</option>
            <option value="ac">AC</option>
            <option value="non_ac">NON-AC</option>
            
          </select>

          <div class="form-group">            
            <label for="number">Number</label>            
            <input class="form-control" type="number" name="number" placeholder="Enter Number" value="" />

            
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    
    @stop






