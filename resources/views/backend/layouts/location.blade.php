@extends('backend.master')
@section('content')
<h1>Location</h1>
<!-- Button -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
	Add Location
</button><br>

<!-- Step-4.2-(session) -->
@if(session()->has('kodeeo'))
<div class="alert alert-success">
	{{ session()->get('kodeeo') }}
</div>
@endif

<br>

<!-- Table -->
<div class="table-responsive">
	<table class="table" id="mytable">
		
		<thead>
			<tr>
				<th scope="col">No.</th>
				<th scope="col">Location Name</th>
				<th scope="col">Address</th>
				<th scope="col">Status</th>
				<th scope="col">Action</th>
			</tr>
		</thead>

		<tbody>

			<!-- Step-5.2 -->
			@foreach($locations as $key=>$singleLocation)
			<tr>
				<td>{{$key+1}}</td> 
				<td>{{$singleLocation->name}}</td>
				<td>{{$singleLocation->address}}</td>
				<td>{{$singleLocation->status}}</td>
				<td>
					<a href=""data-toggle="modal" data-target="#editModal{{$singleLocation->id}}"  class="btn btn-success" >Edit</a>
					
					@method('delete')
					@csrf
					<a href="{{route('location.delete',$singleLocation->id)}}"  class="btn btn-danger" >Delete</a>
				</td>
				<!----------------Edit Modal------------------------> 



				<div class="modal" id="editModal{{$singleLocation->id}}">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Location Update</h5>
								<button class="close" data-dismiss="modal">&times;</button>
							</div>
							<div class="modal-body">
								
								<form method="POST" action="{{route('location.update',$singleLocation->id)}}">
									<!-- Step-4.2-(session) -->
								@if(session()->has('kodeeo'))
								<div class="alert alert-success">
									{{ session()->get('kodeeo') }}
								</div>
								@endif
									@csrf

									<div class="form-group">
										<label for="">Name</label>
										<input class="form-control" type="text" placeholder="Location Name" name="name" value="{{$singleLocation->name}}">
									</div>

									<div class="form-group">            
										<label for="address">Address</label>
										<input class="form-control" type="text" name="address" placeholder="Enter address" value="{{$singleLocation->address}}" />
									</div>

									<!-- dropdown -->
									<div class="form-group">  
										<label>Select Location Status</label>
										<select class="form-control" name="status" >
											<option>Select Status</option>
											<option>Active</option>
											<option>Inactive</option>	
										</select>
									</div>

									<div class="modal-footer">
										<button class="btn btn-success" type="submit">Save</button>
										<button class="btn btn-secondary" data-dismiss="modal">Close</button>

									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

			</div>

			<!----------------Edit Modal------------------------> 
		</tr>
		@endforeach

	</tbody>	
</table>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">

			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Create New Location</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<form method="post" action="{{route('location.create')}}">
				@csrf
				<div class="modal-body">
					<!-- dropdown -->
					<label>Select Location Status</label>
					<select class="form-control" name="location_status" >
						<option>Select Status</option>
						<option>Active</option>
						<option>Inactive</option>	
					</select>

					<label>	Enter Location Name</label>
					<input class="form-control" type="text" name="location_name" placeholder="Enter Location Name Here.." >

					<label>	Enter Location Address</label>
					<input class="form-control" type="text" name="location_adderss" placeholder="Enter Location Details Here.." >

				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>

			</form>
		</div>
	</div>
</div>
@stop