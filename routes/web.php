<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 
// Backend
 
Route::get('/admin', 'Backend\AdminController@admin')->name('admin');
/////////User Management//////////////////////////////////////////////////////////////
Route::get('/user', 'Backend\UserController@user')->name('user');
Route::post('/user/create', 'Backend\UserController@create')->name('user.create');
Route::post('/user/uppdate/{id}','Backend\UserController@update')->name('user.update');
Route::get('/user/delete/{id}','Backend\UserController@destroy')->name('user.delete');

/////////User Management//////////////////////////////////////////////////////////////

/////////Location///////////////////////////////////////////////////////
Route::get('/location', 'Backend\LocationController@location')->name('location');
Route::post('/location/create', 'Backend\LocationController@create')->name('location.create');
Route::post('/location/uppdate/{id}','Backend\LocationController@update')->name('location.update');
Route::get('/location/delete/{id}','Backend\LocationController@destroy')->name('location.delete');


/////////Location///////////////////////////////////////////////////////

Route::get('/booking', 'Backend\BookingController@booking')->name('booking');


////////COUNTER///////////////////////////////////////////////////////////////////////////
Route::get('/counter', 'Backend\CounterController@counter')->name('counter');

Route::post('/counter/create', 'Backend\CounterController@create')->name('counter.create');
Route::post('/counter/update/{id}', 'Backend\CounterController@update')->name('counter.update');
Route::get('/counter/update/{id}', 'Backend\CounterController@delete')->name('counter.delete');

////////COUNTER///////////////////////////////////////////////////////////////////////////

////////TRIP///////////////////////////////////////////////////////////////////////////

Route::get('trip','Backend\TripController@trip')->name('trip');
Route::get('/cut','Backend\TripController@getCounter');
Route::get('/rep','Backend\TripController@getRPCounter');
Route::post('/trip/create','Backend\TripController@create')->name('trip.add');
Route::post('/trip/edit/{id}','Backend\TripController@update')->name('trip.update');
Route::get('/trip/delete/{id}','Backend\TripController@delete')->name('trip.delete');

////////TRIP///////////////////////////////////////////////////////////////////////////

Route::get('/payment', 'Backend\PaymentController@payment')->name('payment');
////////COACH//////////////////////////////////////////////////////////////////////////
Route::get('/coach', 'Backend\CoachController@coach')->name('coach');
Route::post('/coach/create', 'Backend\CoachController@create')->name('coach.create');
Route::post('/coach/update/{id}', 'Backend\CoachController@update')->name('coach.update');
Route::get('/coach/delete/{id}', 'Backend\CoachController@delete')->name('coach.delete');
////////COACH//////////////////////////////////////////////////////////////////////////
