<?php

namespace App\Models;
use App\Models\Location;
use App\Models\Counter;
use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    protected $fillable = ['counter_id','location_id','price','reporting_time','departure_time','departure_counter_id'];

public function location()
{

return $this->belongsTo(Location::class);

}

public function counter()

{

	return $this->belongsTo(Counter::class,);

}

    
}
