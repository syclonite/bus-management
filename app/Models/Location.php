<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable=['name','address', 'status'];

 
// public function trip()
// {
// return $this->belongsTo(Trip::class,'id','location_id','counter_id','departure_counter_id');
// }

public function counter()
{
return $this->belongsTo(Counter::class);
}



}
