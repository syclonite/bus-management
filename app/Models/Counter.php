<?php

namespace App\Models;
use App\Models\Location;
use Illuminate\Database\Eloquent\Model;


class Counter extends Model
{
    protected $fillable=['location_id','name','address','mobile'];


public function location()
{

	return $this->belongsTo(Location::class);

}


}

