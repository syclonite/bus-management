<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Trip;
use App\Models\Location;
use App\Models\Counter; 
use DB;
use Illuminate\Support\Facades\Input;
class TripController extends Controller
{


    public function trip()
    
     {  
        $counter = Counter::get();
        $trip = Trip::get();
        $location = DB::table('locations')->pluck("name","id");
        // dd($location);
        return view('backend.layouts.trip',compact('location','trip','counter'));
    }

 public function getCounter(Request $r) 
{        
        
          $id =$r->id;

          if($id){
            
            $counters = DB::table("counters")->where("location_id",$id)->pluck("name","id"); 
          }
          

        return json_encode( $counters);
}

 public function getRPCounter(Request $r) 
{        
        
          $id =$r->id;

          if($id){
            
            $rp_counters = DB::table("counters")->where("location_id",$id)->pluck("name","id"); 
          }
          

        return json_encode( $rp_counters);
}




    public function create(Request $request)
    {	
    	// dd($request->all());

    	$trip = [

    		'counter_id'=>$request->input('counter_id'),
    		'location_id'=>$request->input('location'),
    		'price'=>$request->input('price'),
    		'reporting_time'=>$request->input('r_time'),
    		'departure_time'=>$request->input('d_time'),
    		'departure_counter_id'=>$request->input('dc_name'),

    	];

    	Trip::create($trip);
    	return redirect()->back()->with('kodeeo','Trip Created Successfully');

    }

    public function update(Request $request,$id)

    {
        $trip = [

            'counter_id'=> $request->input('counter_id'),
            'location_id'=> $request->input('location'),
            'price'=> $request->input('price'),
            'reporting_time'=>$request->input('r_time'),
            'departure_time'=>$request->input('d_time'),
            'departure_counter_id'=>$request->input('dp_counter'),

        ];


        Trip::find($id)->update($trip);

        return redirect()->back()->with('kodeeo','Trip Updated Successfully');;


    }

 
    public function delete($id)
    {

        Trip::where('id',$id)->delete();
        return redirect()->back()->with('kodeeo','Trip Deleted Successfully');
    }

} 
