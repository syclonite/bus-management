<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Counter;
use App\Models\Location;

class CounterController extends Controller
{
    public function counter()

    {

        $counter = Counter::get();
        $location= Location::get();
    	return view('backend.layouts.counter',compact('counter','location'));

    }

    public function create(Request $request)
    {
        
        Counter::create([
    		 
    		'location_id'=>$request->input('location_id'),
    		'address'=>$request->input('counter_address'),
    		'name'=>$request->input('counter_name'),
    		'mobile'=>$request->input('counter_mobile_number'),
	    ]);
		
		return redirect()->back()->with('kodeeo','Counter Created Successfully');
	 }

     public function update(Request $request,$id)

     {
       $data=[

            'location_id'=>$request->input('location_id'),
            'address'=>$request->input('counter_address'),
            'name'=>$request->input('counter_name'),
            'mobile'=>$request->input('counter_mobile_number'),

       ];

       Counter::where('id',$id)->update($data);
       return redirect()->back()->with('kodeeo','Counter updated Successfully');

     }
    		
     public function delete($id)
     {

        Counter::where('id',$id)->delete();
        return redirect()->back()->with('kodeeo','Counter Deleted Successfully');

     }


    	
}
