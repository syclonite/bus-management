<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Coach;

class CoachController extends Controller
{
    public function coach()
    {	
    	$coach=Coach::get();
        return view('backend.layouts.coach',compact('coach'));
    }
    public function create(Request $request)

    {        
    	$coach=[

    		'type'=>$request->input('type'),
    		'number'=>$request->input('number'),

    	];

    	Coach::create($coach);
    	return redirect()->back()->with('kodeeo','Coach Created Successfully');


    }


    public function update(Request $request,$id)

    {
    		
    	$coach=[

    		'type'=>$request->input('type'),
    		'number'=>$request->input('number'),

    	];

    	Coach::where('id',$id)->update($coach);
    	return redirect()->back()->with('kodeeo','Coach Updated Successfully');


    }
    
    public function delete($id)

    {
    	Coach::where('id',$id)->delete();
    	return redirect()->back()->with('kodeeo','Coach Deleted Successfully');

    }



}
