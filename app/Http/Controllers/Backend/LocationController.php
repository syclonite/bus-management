<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Location;

class LocationController extends Controller
{
    public function location()
    {
    	// Step-5.1
    	$locations=Location::all();

        // Step-5.2(compact('locations'))
    	return view('backend.layouts.location',compact('locations')); 

    }
    public function create(Request $request)
    {
    	// Step-3
        Location::create([
    		'name'=>$request->input('location_name'),
    		'address'=>$request->input('location_adderss'),
    		'status'=>$request->input('location_status'),
    	]);
    	// step-2
    	// dd($request->all());

    	return redirect()->back()->with('kodeeo','Location Created Successfully');
    }

     public function update(Request $request,$id)
    {
    	// dd($request->all());

      $location =[

            'name'=>$request->input('name'),
            'address'=>$request->input('address'),
            'status'=>$request->input('status'),
      ];

      Location::where('id',$id)->update($location);
     return redirect()->back()->with('kodeeo','Location updated Successfully');

    }

    public function destroy($id)

    {
        Location::where('id',$id)->delete();

        return redirect()->back()->with('kodeeo','Location Deleted Successfully');


    }
}

