<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;

class UserController extends Controller
{
    public function user()
    { 
        $user = User::get();
    	return view('backend.layouts.user',compact('user'));
    }


    public function create(Request $request)
    {
    	// dd($request->all());

    	User::create([
    		'name'=>$request->input('name'),
            'role'=>$request->input('type'),
    		'email'=>$request->input('email'),
    		'mobile'=>$request->input('phone'),
    		'address'=>$request->input('address'),
    		'password'=>bcrypt($request->input('password')),
    		
    	]);

        return redirect()->back()->with('kodeeo','User Created Successfully');

    }

    public function update(Request $request,$id)
    {

      $user = [

            'name'=>$request->input('name'),
            'email'=>$request->input('email'),
            'mobile'=>$request->input('phone'),
            'address'=>$request->input('address'),

      ];

      User::where('id',$id)->update($user);
      return redirect()->back()->with('kodeeo','User updated Successfully');

    }

    public function destroy($id)

    {
        User::where('id',$id)->delete();

        return redirect()->back()->with('kodeeo','User Deleted Successfully');

    }



}
