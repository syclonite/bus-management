<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use App\Models\Location;

$factory->define(Location::class, function (Faker $faker) {
    return [
        'name' => $faker->country,
        'address' => $faker->address,
        'status'=>$faker->randomElement(['Active','Inactive']),
        'remember_token' => Str::random(20),
        // 'count_id' => rand(1,10)
        
    ];
});


