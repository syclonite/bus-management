<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Trip;
use Faker\Generator as Faker;

$factory->define(Trip::class, function (Faker $faker) {
    return [
        'counter_id' => rand(1,20),
        'location_id' => rand(1,20),
        'reporting_time' =>$faker->time($format = 'H:i:s', $max = 'now'),
        'departure_time' => $faker->time($format = 'H:i:s', $max = 'now'),
        'departure_counter_id' => rand(1,20),
        'price' => $faker->numberBetween(1,20),
        'remember_token' => Str::random(20),
        
    ];
});
