<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Rand\Generator as rand;
use App\Models\Coach;

$factory->define(Coach::class, function (Faker $faker) {
    return [
        'number' =>$faker->unique()->numberBetween(1,20),
        'type'=>$faker->randomElement(['AC','Non-AC']),
        'remember_token' => Str::random(20),
        
    ];
});
