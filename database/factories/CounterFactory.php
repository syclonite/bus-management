<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Counter;
use Faker\Generator as Faker;

$factory->define(Counter::class, function (Faker $faker) {
    return [
        'name' => $faker->state,
        'mobile' =>$faker->phoneNumber,
        'location_id' => rand(1,20),
		'address' => $faker->address,
        'remember_token' => Str::random(20),
        
    ];
});
