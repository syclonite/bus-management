<?php

use Illuminate\Database\Seeder;
use App\Models\Counter;

class CountersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Counter::class,20)->create();
    }
}
