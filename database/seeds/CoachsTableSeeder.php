<?php

use Illuminate\Database\Seeder;
use App\Models\Coach;

class CoachsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Coach::class,20)->create();
    }
}
