<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Location;
use App\Models\Coach;
use App\Models\Counter;
use App\Models\Trip;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(LocationsTableSeeder::class);
        $this->call(CoachsTableSeeder::class);
        $this->call(CountersTableSeeder::class);
        $this->call(TripsTableSeeder::class);
    }
}
