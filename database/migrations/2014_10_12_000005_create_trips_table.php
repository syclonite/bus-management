<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::enableForeignKeyConstraints();
        Schema::create('trips', function (Blueprint $table) {
            $table->Increments('id')->unsigned()->nullable();
            $table->integer('counter_id')->unsigned()->nullable();
            $table->integer('location_id')->unsigned()->nullable();
            $table->string('price')->nullable();
            $table->string('reporting_time')->nullable();
            $table->string('departure_time')->nullable();
            $table->integer('departure_counter_id')->unsigned()->nullable();
            $table->timestamps();
            $table->rememberToken();
            
            //foreign key//
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');
            $table->foreign('counter_id')->references('id')->on('counters')->onDelete('cascade');
            $table->foreign('departure_counter_id')->references('id')->on('counters')->onDelete('cascade');
            //foreign key//

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
