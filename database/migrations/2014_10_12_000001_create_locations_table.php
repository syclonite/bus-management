<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()

    {
       Schema::enableForeignKeyConstraints();
        Schema::create('locations', function (Blueprint $table) {
            $table->Increments('id')->unsigned()->nullable();
            $table->string('name');
            $table->text('address');
            $table->string('status')->default("Active");
            $table->remembertoken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
