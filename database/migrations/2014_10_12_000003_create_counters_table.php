<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   Schema::enableForeignKeyConstraints();
        Schema::create('counters', function (Blueprint $table) {
            $table->Increments('id')->unsigned()->nullable();
            $table->integer('location_id')->unsigned()->nullable();
            $table->string('name');
            $table->text('address');
            $table->string('mobile');
            $table->timestamps();
            $table->rememberToken();
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('counters');
    }
}
